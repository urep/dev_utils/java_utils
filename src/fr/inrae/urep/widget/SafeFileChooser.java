package fr.inrae.urep.widget;

import java.io.File;
import java.nio.file.Path;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 * This class provides a "safe" version of a FileChooser.
 * Here, "safe" mean that the user will  be warned if he try to
 * overwrite an existing file.
 * @author INRA UREP
 */
public class SafeFileChooser extends JFileChooser {
	/**
	 * Constructs a SafeFileChooser pointing to the user's default directory.
	 * This default depends on the operating system. It is typically the
	 * "My Documents" folder on Windows, and the user's home directory on Unix.
	 */
	public SafeFileChooser() {
		super();
	}

	/**
	 * Constructs a SafeFileChooser using the given path.
	 * @param currentDirectoryPath String specifying the path to a directory.
	 */
	public SafeFileChooser(String currentDirectoryPath) {
		super(currentDirectoryPath);
	}

	/**
	 * Constructs a SafeFileChooser using the given path.
	 * @param currentDirectoryPath Path object specifying the path to a
	 * directory.
	 */
	public SafeFileChooser(Path currentDirectoryPath) {
		super(currentDirectoryPath.toString());
	}

	/**
	 * Constructs a SafeFileChooser using the given path.
	 * @param currentDirectory File object specifying the path to a
	 * directory.
	 */
	public SafeFileChooser(File currentDirectory) {
		super(currentDirectory);
	}

	@Override
	public void approveSelection() {
		if (getDialogType() == SAVE_DIALOG) {
			File file = getSelectedFile();
			if (file != null && file.exists()) {
				int answer = Dialogs.warningQuestion(this, "The file "
						+ file.getName() + " already exists. Overwrite?",
						"Confirmation");
				if (answer != JOptionPane.YES_OPTION)
					return;
			}
		}
		super.approveSelection();
	}

	/** Serial version unique identifier. */
	private static final long serialVersionUID = 2841621822854221055L;
}
