package fr.inrae.urep.widget;

import java.awt.Component;

import javax.swing.JOptionPane;

/**
 * An utilitary class to display informative dialog boxes.
 * @author INRA UREP
 */
public class Dialogs {
    /**
     * Display a dialog with an error message.
     * @param parent Frame in which the dialog is displayed.
     * @param msg    Error message.
     * @param kind   Kind of error.
     */
    public static void error(Component parent, String msg, String kind) {
    	JOptionPane.showMessageDialog(parent, msg, kind,
    				                  JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Display a warning dialog with a question.
     * @param parent   Frame in which the dialog is displayed.
     * @param question Question.
     * @param title    Dialog title.
     * @return JOptionPane.YES_OPTION if the user answer yes.
     */
    public static int warningQuestion(Component parent, String question, String title) {
    	return JOptionPane.showConfirmDialog(parent, question, title,
    			JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
    }
}
