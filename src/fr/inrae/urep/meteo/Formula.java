package fr.inrae.urep.meteo;

public class Formula {
    /**
     * Computes the saturation vapor pressure.
     * @param temperature temperature (in °C).
     * @return the saturation vapor pressure (in hPa).
     */
    public static double bolton1980(double temperature) {
		// Equation 10 in [1].
        return 6.112*Math.exp((17.67*temperature) / (temperature+243.5));
    }
}

/*
 * References:
 * [1]: Bolton, D. (1980) : The computation of equivalent potential temperature. Monthly Weather Review, 108, 1046-1053
 */