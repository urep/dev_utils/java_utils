package fr.inrae.urep.meteo;

/**
 * This class provides methods to disaggregate daily meteorological data into
 * hourly data, according to the algorithms used in PaSim.
 * @author INRA UREP
 */
public class DailyToHourly {
	/**
	 * Converts daily rainfall into hourly rainfall.
	 *
	 * Formula from PaSim source code (day_year.f90, subroutine getinputdaily2hourly).
	 *
	 * @param dailyPa daily rainfall. Must be in mm/d.
	 * @return rainfall (in mm/d).
	 *
	 * @.precond dailyPa must have a size multiple of 365.
	 */
	public static Double[] convertRainfall(Double[] dailyPa) {
		// Precondition checks.
		if (dailyPa.length % 365 != 0)
			throw new AssertionError("The number of days  is not a multiple of 365");

		Double[] hourlyPa = new Double[dailyPa.length * 24];
		int nbYears = dailyPa.length / 365;
		int time = 0;
		for (int year = 0; year < nbYears; ++year) {
			for (int day = 0; day < 365; ++day) {
				int realDay = year*365 + day;
				for (int hour = 0; hour < 24; hour++) {
					// ATTENTION : les donnees de precipition en entree de
					// PASIM sont en mm/jour ce qui pourrait s'apparenter a
					// une vitesse de precipitation plutot qu'a une quantite.
					hourlyPa[time] = dailyPa[realDay];
					++time;
				}
			}
		}
		return hourlyPa;
	}

	/**
	 * Converts daily min/max air temperature into hourly average air temperature.
	 *
	 * Formula from PaSim source code (day_year.f90, subroutine getinputdaily2hourly).
	 *
	 * @param dailyTamin daily min air temperature. Must be in degree Celsius.
	 * @param dailyTamax daily max air temperature. Must be in degree Celsius.
	 * @param latitude   latitude (in radian).
	 * @return average air temperature (in K).
	 *
	 * @.precond dailyTamin and dailyTamax must have the same length.
	 * @.precond input arrays must have a size multiple of 365.
	 */
	public static Double[] convertTemperature(Double[] dailyTamin, Double[] dailyTamax, double latitude) {
		// Precondition checks.
		if (dailyTamax.length % 365 != 0)
			throw new AssertionError("The number of days  is not a multiple of 365");
		if (!(dailyTamin.length == dailyTamax.length))
			throw new AssertionError("Different sizes of input arrays.");

		Double[] hourlyTa = new Double[dailyTamax.length * 24];
		int nbYears = dailyTamin.length / 365;
		double toKelvin = 273.15;
		int time = 0;
		for (int year = 0; year < nbYears; ++year) {
			for (int day = 0; day < 365; ++day) {
				int realDay = year*365 + day;

				if (dailyTamax[realDay] == null || dailyTamin[realDay] == null) {
					for (int hour = 0; hour < 24; hour++)
						hourlyTa[time] = null;
				}
				else {
					for (int hour = 0; hour < 24; hour++) {
						double declSinNoon = -Math.sin(0.409)*Math.cos(Math.PI*2*(realDay + 1 + 0.5 + 10)/365);
						double declCosNoon =  Math.sqrt(1-declSinNoon*declSinNoon);
						double xCoord      = -(Math.sin(latitude)* declSinNoon) / ( Math.cos(latitude) * declCosNoon);
						double dayLength   = 24 * 2 * Math.atan2(Math.sqrt((1.0-xCoord*xCoord)),xCoord)/(2.0*Math.PI);
						double tdawn=(24-dayLength)/2;
						double Tamean = (dailyTamax[realDay] + dailyTamin[realDay]) / 2;
						double taVar =  (dailyTamax[realDay] - dailyTamin[realDay]) / 2;
						if(hour < tdawn)
							hourlyTa[time] = Tamean-taVar*Math.sin(2*Math.PI*(hour+25-(24+tdawn+15)/2)) + toKelvin;
						else if(hour < 15)
							hourlyTa[time] = Tamean+taVar*Math.sin(2*Math.PI*(hour+1-(15+tdawn)/2)/(2*(15-tdawn))) + toKelvin;
						else
							hourlyTa[time] = Tamean-taVar*Math.sin((2*Math.PI*(hour+1-(15+24+tdawn)/2)/(2*(24+tdawn-15)))) + toKelvin;
						++time;
					}
				}
			}
		}
		return hourlyTa;
	}

	/**
	 * Converts daily radiation into hourly radiation.
	 *
	 * Formula from PaSim source code (day_year.f90, subroutine getinputdaily2hourly).
	 *
	 * @param dailyIATMtot radiation. Must be in J/cm2.
	 * @param latitude     latitude (in radian).
	 * @return radiation (in W/m2).
	 *
	 * @.precond dailyIATMtot must have a size multiple of 365.
	 */
	public static Double[] convertRadiation(Double[] dailyIATMtot,
			                                double latitude) {
		// Precondition checks.
		if (dailyIATMtot.length % 365 != 0)
			throw new AssertionError("The number of days  is not a multiple of 365");

		Double[] hourlyIATMtot = new Double[dailyIATMtot.length * 24];
		int nbYears = dailyIATMtot.length / 365;
		int time = 0;
		for (int year = 0; year < nbYears; ++year) {
			for (int day = 0; day < 365; ++day) {
				int realDay = year*365 + day;

				if (dailyIATMtot[realDay] == null) {
					for (int hour = 0; hour < 24; hour++)
						hourlyIATMtot[time] = null;
				}
				else {
					double declsinnoon = -Math.sin(0.409)*Math.cos(Math.PI*2.0*((day+1) + 0.5 + 10.0)/365.0);
					double declcosnoon = Math.sqrt(1.0 - declsinnoon*declsinnoon);
					double tdawn = -Math.sin(latitude)*declsinnoon / Math.cos(latitude)/declcosnoon;
					double daylength;
					if (Math.abs(1.0 - tdawn*tdawn) > 5E-1)
						daylength = 24.0*2.0*Math.atan2(Math.sqrt(1.0 - tdawn*tdawn), tdawn)/(2.0*Math.PI);
					else
						daylength = 23;
					tdawn = (24.0 - daylength)/2.0;
					for (int hour = 0; hour < 24; hour++) {
						if(hour < tdawn)
							hourlyIATMtot[time] = 0.0;
						else if(hour < (24-tdawn)) {
							hourlyIATMtot[time] = dailyIATMtot[realDay] / (daylength*3600)*(1.0 + Math.cos(2*Math.PI*(hour-12.0)/daylength));
							hourlyIATMtot[time] = Math.max(hourlyIATMtot[time]*10000 ,0.0);
						}
						else
							hourlyIATMtot[time] = 0.0;
						++time;
					}
				}
			}
		}
		return hourlyIATMtot;
	}

	/**
	 * Converts daily wind speed into hourly wind speed.
	 *
	 * Formula from PaSim source code (day_year.f90, subroutine getinputdaily2hourly).
	 *
	 * @param dailyU daily Wind Speed. Must be in m/s.
	 * @return wind speed (in m/s).
	 *
	 * @.precond dailyU must have a size multiple of 365.
	 */
	public static Double[] convertWindSpeed(Double[] dailyU) {
		// Precondition checks.
		if (dailyU.length % 365 != 0)
			throw new AssertionError("The number of days  is not a multiple of 365");

		Double[] hourlyU = new Double[dailyU.length * 24];
		int nbYears = dailyU.length / 365;

		int time = 0;
		for (int year = 0; year < nbYears; ++year) {
			for (int day = 0; day < 365; ++day) {
				int realDay = year*365 + day;
				for (int hour = 0; hour < 24; hour++) {
					hourlyU[time] = dailyU[realDay];
					++time;
				}
			}
		}
		return hourlyU;
	}

	/**
	 * Converts daily relative humidity into hourly water vapour pressure.
	 *
	 * Formula from PaSim source code (day_year.f90, subroutine getinputdaily2hourly).
	 *
	 * @param dailyRH    relative Humidity. Must be in %.
	 * @param dailyTamin daily min air temperature. Must be in degree Celsius.
	 * @param dailyTamax daily max air temperature. Must be in degree Celsius.
	 * @return hourly water vapour pressure (in kPa).
	 *
	 * @.precond dailyRH, dailyTamin and dailyTamax must have the same length.
	 * @.precond input arrays must have a size multiple of 365.
	 */
	public static Double[] convertRelativeHumidity(Double[] dailyRH,
												   Double[] dailyTamin,
												   Double[] dailyTamax) {
		// Precondition checks.
		if (dailyRH.length % 365 != 0)
			throw new AssertionError("The number of days  is not a multiple of 365");
		if (!(dailyRH.length    == dailyTamin.length) &&
			 (dailyTamin.length == dailyTamax.length))
			throw new AssertionError("Different sizes of input arrays.");

		Double[] hourlyEa = new Double[dailyRH.length * 24];
		int nbYears = dailyRH.length / 365;

		int time = 0;
		for (int year = 0; year < nbYears; ++year) {
			for (int day = 0; day < 365; ++day) {
				int realDay = year*365 + day;

				if (dailyTamax[realDay] == null || dailyTamin[realDay] == null || dailyRH[realDay] == null) {
					for (int hour = 0; hour < 24; hour++)
						hourlyEa[time] = null;
				}
				else {
					double Ta = (dailyTamax[realDay] + dailyTamin[realDay]) / 2;
					double dailyEA =0.6112*Math.exp((17.67*Ta)/(Ta+243.5)) * (dailyRH[realDay]/100);
					for (int hour = 0; hour < 24; hour++) {
						hourlyEa[time] = dailyEA;
						++time;
					}
				}
			}
		}
		return hourlyEa;
	}
}
