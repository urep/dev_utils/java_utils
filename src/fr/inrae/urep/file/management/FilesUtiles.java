package fr.inrae.urep.file.management;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class FilesUtiles {

	public static boolean createDirectoryIfNotExist (Path pathFolder ){
		if(Files.notExists(pathFolder)){
			try {
				Files.createDirectories(pathFolder);
				return true;
			} catch(IOException e) {
				System.err.println("Failed to create directory!" + e.getMessage());
			}
		}
		return false;
	}
}
