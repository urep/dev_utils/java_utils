package fr.inrae.urep.math;

/**
 * This class provides methods to compute some statistical indicators.
 * @author INRA UREP
 */
public class Statistic {
    /**
     * Computes the mean value of a vector.
     * @param vector the vector.
     * @return the mean value.
     * @.precond vector must be non-empty.
     */
    static public double mean(double[] vector) {
    	assert vector.length > 0 : "Empty vector";
        double mean = 0.0;

        for(double value : vector)
            mean += value;
        mean /= vector.length;

        return mean;
    }
    
    /**
     * Computes the sum value of a vector.
     * @param vector the vector.
     * @return the sum value.
     * @.precond vector must be non-empty.
     */
    static public double sum(double[] vector) {
    	assert vector.length > 0 : "Empty vector";
        double sum = 0.0;

        for(double value : vector)
            sum += value;

        return sum;
    }

    /**
     * Computes the standard deviation of a vector, using the precomputed mean.
     * @param vector the vector.
     * @param mean the mean value of the vector.
     * @return the standard deviation.
     * @.precond vector must contains at least two elements.
     */
    static public double standardDeviation(double[] vector, double mean) {
    	assert vector.length > 1 : "Vector too small.";
        double standardDeviation = 0.0;

        for(double value : vector)
            standardDeviation += ((value - mean)*(value - mean));

        return Math.sqrt(standardDeviation / (vector.length - 1));
    }

    /**
     * Computes the standard deviation of a vector.
     * @param vector the vector.
     * @return the standard deviation.
     * @.precond vector must contains at least two elements.
     */
    static public double standardDeviation(double[] vector) {
        return standardDeviation(vector,  mean(vector));
    }

    /**
     * Computes the coefficient of variation of a vector.
     * @param vector the vector.
     * @return the coefficient of variation.
     * @.precond vector must contains at least two elements.
     */
    static public double coefficientOfVariation(double[] vector) {
        double mean = mean(vector);
        double standardDeviation = standardDeviation(vector, mean);

        return coefficientOfVariation(mean, standardDeviation);
    }

    /**
     * Computes the standard deviation of a vector, using the precomputed mean
     * and standard deviation.
     * @param mean              the mean value of the vector.
     * @param standardDeviation the standard deviation of the vector.
     * @return the coefficient of variation.
     */
    static public double coefficientOfVariation(double mean,
                                                double standardDeviation) {
        return standardDeviation / mean;
    }
}
