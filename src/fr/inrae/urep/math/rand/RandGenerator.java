package fr.inrae.urep.math.rand;

import java.util.Random;

/**
 * This class is the standard class for a random generator
 * @author INRA UREP
 */
public class RandGenerator {
	/** The default seed */
	protected static final int DEFAULT_SEED = 1;
	
	/** The seed used to initialized the generator */
	protected int seed;
	
	/** the random generator */
	protected Random randomGenerator;
	
	/**
	 * Create the java classic random generator with the default seed
	 */
	public RandGenerator(){
		this(DEFAULT_SEED);
	}

	/**
	 * Create the java classic random generator with the given seed
	 * @param inSeed the seed of the random generator parameter
	 */
	public RandGenerator(int inSeed) {
		seed = inSeed;
		randomGenerator = new Random(seed);
	}
	
	/**
	 * Return the next pseudo random integer in [0; inMax[
	 * @param inMax the maximum value (exclusive) of the random int to return
	 * @return the next pseudo random integer in [0; inMax[
	 */
	final public int nextInt(int inMax){
		return randomGenerator.nextInt(inMax);
	}
	
}
