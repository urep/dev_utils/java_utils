package fr.inrae.urep.math;

import fr.inrae.urep.CloneableObject;

/**
 * A standard matrix
 * @author INRA UREP
 */
final public class Matrix<E> {
	/** number of rows */
	private final int M;
	
	/** number of columns */
    private final int N;
    
    /** M-by-N array */
    private final Object[][] data;

    /**
     * create M-by-N matrix of empty object
     * @param M the number of rows of the matrix
     * @param N the number of columns of the matrix
     */
    public Matrix(int M, int N) {
        this.M = M;
        this.N = N;
        data = new Object[M][N];
    }

    /**
     * Create matrix based on 2d array
     * @param data the 2D array
     */
    public Matrix(Object[][] data) {
        M = data.length;
        N = data[0].length;
        this.data = new Object[M][N];
        for (int i = 0; i < M; i++)
            for (int j = 0; j < N; j++)
                    this.data[i][j] = data[i][j];
    }

    /**
     * Swap rows i and j
     * @param i the index of the first row to swap
     * @param j the index of the second row to swap
     */
    public void swap(int i, int j) {
        Object[] temp = data[i];
        data[i] = data[j];
        data[j] = temp;
    }

    /**
     * Create and return the transpose of the invoking matrix
     * @return the transposed matrix
     */
    public Matrix<E> transpose() {
        Matrix<E> A = new Matrix<E>(N, M);
        for (int i = 0; i < M; i++)
            for (int j = 0; j < N; j++)
                A.data[j][i] = this.data[i][j];
        return A;
    }


    /**
     * Does A = B exactly?
     * @param B the matrix to compare
     * @return true if A == B false elsewhere
     */
    public boolean eq(Matrix<E> B) {
        Matrix<E> A = this;
        if (B.M != A.M || B.N != A.N) 
        	throw new RuntimeException("Illegal matrix dimensions.");
        for (int i = 0; i < M; ++i)
            for (int j = 0; j < N; ++j)
                if (!A.data[i][j].equals(B.data[i][j])) 
                	return false;
        return true;
    }

    /**
     * Print matrix to standard output
     */
    public void show() {
        for (int i = 0; i < M; ++i) {
            for (int j = 0; j < N; ++j) 
                System.out.print("[" + data[i][j].toString() + "]");
            System.out.println();
        }
    }

    
    /**
     * Return the element (row, column)
     * @param row the row number of the element 
     * @param column the column number of the element
     * @return the element (row, column)
     */
    @SuppressWarnings("unchecked")
	final public E get(int row, int column){
    	return (E)data[row][column];
    }
    
    /**
     * Fill the matrix with the cloneable object given in parameter
     * @param object the object to clone and to put in each cell of the matrix
     */
    final public void fillWith(CloneableObject object ){
        for (int i = 0; i < M; ++i) {
            for (int j = 0; j < N; ++j) 
                data[i][j] = object.clone();
        }
    }
    
    /**
     * Return the number of columns
     * @return the number of columns
     */
    final public int getColumnNumber(){
    	return N;
    }
    
    /**
     * Return the number of rows
     * @return the number of rows
     */
    final public int getRowNumber(){
    	return M;
    }
}
