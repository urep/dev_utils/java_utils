package fr.inrae.urep.math;

/**
 * Some useful formula
 * @author INRA UREP
 */
public class Formula {
	
	/**
	 * Return the distance between (x0,y0) and (x1, y1)
	 * @param x0 the x coordinate of the first point 
	 * @param y0 the y coordinate of the first point
	 * @param x1 the x coordinate of the second point
	 * @param y1 the y coordinate of the second point
	 * @return the distance between (x0,y0) and (x1, y1)
	 */
	public static final double GET_DISTANCE(double x0, double y0, double x1, double y1){
		return Math.pow((x0-x1)*(x0-x1) + (y0-y1)*(y0-y1), 0.5);
	}
}
