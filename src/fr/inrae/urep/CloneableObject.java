package fr.inrae.urep;

/**
 * This interface allox to indicate that an object expose its clone() method
 * @author INRA UREP
 */
public interface CloneableObject extends Cloneable {
	/**
	 * Return a deep copy of the current object
	 * @return a deep copy of the current object
	 */
	public Object clone();
}
