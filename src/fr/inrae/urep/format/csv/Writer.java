package fr.inrae.urep.format.csv;

import java.io.Closeable;
import java.io.IOException;
import java.util.List;

import com.opencsv.CSVWriter;
import com.opencsv.CSVWriterBuilder;
import com.opencsv.ICSVWriter;

/**
 * This class implements a simple CSV writer that can be used to write user's
 * data into delimited strings into a CSV file.
 * @author INRA UREP
 */

/*
 * This class is a simple wrapper for the CSVWriter of OpenCSV.
 * http://opencsv.sourceforge.net/
 */
public class Writer implements Closeable {
    /**
     * Constructs Writer that uses the default dialect.
     * 
     * @param writer the writer to an underlying CSV source.
     */
    public Writer(java.io.Writer writer) {
        this(writer, new Dialect());
    }

    /**
     * Constructs Writer with supplied dialect.
     * 
     * @param writer  the writer to an underlying CSV source.
     * @param dialect the dialect to use to parse the CSV.
     */
    public Writer(java.io.Writer writer, Dialect dialect) {
    	this.writer = (CSVWriter) new CSVWriterBuilder(writer)
    		    .withSeparator(dialect.delimiter)
    		    .withQuoteChar(dialect.quotechar)
    		    .withEscapeChar(dialect.escape)
    		    .withLineEnd(ICSVWriter.DEFAULT_LINE_END)
    		    .build(); // will produce a CSVWriter
    }

    /**
     * Writes a List of rows into a file.
     * 
     * @param rows a List of String[], with each String[] representing a row.
     */
    public void writeAll(List<String[]> rows) {
        writer.writeAll(rows);
    }

    /**
     * Writes a row into a file.
     * 
     * @param row a string array with one column per cell.
     */
    public void writeRow(String[] row) {
        writer.writeNext(row);
    }

    /**
     * Closes the underlying writer.
     * 
     * @throws IOException if the close fails.
     */
    public void close() throws IOException {
        writer.close();
    }

    /** The CSV writer. */
    private CSVWriter writer;
}
