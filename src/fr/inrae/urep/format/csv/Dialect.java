/**
 *
 */
package fr.inrae.urep.format.csv;

/**
 * This class makes it easier to specify the format of input and output records
 * by grouping together the specific formatting parameters.
 * @author INRA UREP
 */
public class Dialect {

    /** The character constant to use when you do not want define a character. */
    public static final char NOTHING = '\0';

    /**
     * Constructs default Dialect that uses a comma as delimiter, the double
     * quote as quote character and the backslash as escape character.
     */
    public Dialect() {
        delimiter = ',';
        quotechar = '"';
        escape = '\\';
    }

    /**
     * Set the delimiter character.
     *
     * @param delimiter delimiter to use for separating columns.
     * @return the dialect object.
     * @throws CSVException if the delimiter character is equal to the quote or
     * the escape character.
     *
     * @.postcond The separator, quote, and escape characters must be
     * different.
     */
    public Dialect setDelimiter(char delimiter) throws CSVException {
    	char old_delimiter = this.delimiter;
        this.delimiter = delimiter;

        if(!isValid()) {
        	this.delimiter = old_delimiter;
        	throw new CSVException("The separator, quote, and escape " +
        	                       "characters must be different");
        }

        return this;
    }

    /**
     * Set the quoting character.
     *
     * @param quotechar character to use for quoted elements.
     * @return the dialect object.
     * @throws CSVException if the quoting character is equal to the separator
     * or the escape character.
     *
     * @.postcond The separator, quote, and escape characters must be
     * different.
     */
    public Dialect setQuotechar(char quotechar) throws CSVException {
    	char old_quotechar = this.quotechar;
        this.quotechar = quotechar;

        if(!isValid()) {
        	this.quotechar = old_quotechar;
           	throw new CSVException("The separator, quote, and escape " +
                    "characters must be different");
        }

        return this;
    }

    /**
     * Set the escape character.
     *
     * @param escape character to use for escaping separator or quote.
     * @return the dialect object.
     * @throws CSVException if the escape character is equal to the separator
     * or the quote character.
     *
     * @.postcond The separator, quote, and escape characters must be
     * different.
     */
    public Dialect setEscape(char escape) throws CSVException {
    	char old_escape = this.escape;
        this.escape = escape;

        if(!isValid()) {
        	this.escape = old_escape;
           	throw new CSVException("The separator, quote, and escape " +
                    "characters must be different");
        }

        return this;
    }

    /**
     * Checks if the dialect is valid: the delimiter is a valid character,
     * the escaping character and the delimiter are different, ...
     */
    private boolean isValid() {
        if ((delimiter != NOTHING && delimiter == quotechar) ||
            (delimiter != NOTHING && delimiter == escape)    ||
            (quotechar != NOTHING && quotechar == escape))
            return false;
        else
            return true;
    }

    /**
     * The delimiter to use for separating columns.
     */
    protected char delimiter;
    /**
     * The character to use for quoted elements.
     */
    protected char quotechar;
    /**
     * The character to use for escaping separator or quote.
     */
    protected char escape;
}
