/**
 *
 */
package fr.inrae.urep.format.csv;

import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvException;
import com.opencsv.exceptions.CsvValidationException;


/**
 * This class implements a simple CSV reader that can be used to "slurp" the
 * whole file in one time or to use it in an Iterator style pattern.
 * This reader maps the information read into a Map whose keys are the field
 * names.
 * @author INRA UREP
 */
public class DictReader implements Closeable {
    /**
     * Constructs DictReader that uses the default dialect and uses the first
     * row of the CSV as the field names.
     *
     * @param reader the reader to an underlying CSV source.
     *
     * @throws IOException if something goes wrong during reading the first
     * row.
     * @throws CsvValidationException 
     */
    public DictReader(java.io.Reader reader) throws IOException, CsvValidationException {
        this(reader, new Dialect());
    }

    /**
     * Constructs DictReader with supplied dialect and uses the first row of
     * the CSV as the field names.
     *
     * @param reader  the reader to an underlying CSV source.
     * @param dialect the dialect to use to parse the CSV.
     *
     * @throws IOException if something goes wrong during reading the first
     * row.
     * @throws CsvValidationException 
     */
    public DictReader(java.io.Reader reader, Dialect dialect) throws CsvValidationException, IOException {
        final CSVParser parser = new CSVParserBuilder()
        		.withSeparator(dialect.delimiter)
        		.withEscapeChar(dialect.escape)
        		.withQuoteChar(dialect.quotechar)
        		.withIgnoreQuotations(true)
        		.build();
        this.reader = new CSVReaderBuilder(reader)
        		.withCSVParser(parser)
        		.build();
        fieldNames = this.reader.readNext();
    }

    /**
     * Constructs DictReader with supplied field names and default dialect.
     *
     * @param reader     the reader to an underlying CSV source.
     * @param fieldNames the list of field names.
     *
     * @.precond fieldNames must contains one name per column (not less).
     */
    public DictReader(java.io.Reader reader, String[] fieldNames) {
        this(reader, new Dialect(), fieldNames);
    }

    /**
     * Constructs DictReader with supplied field names and default dialect.
     *
     * @param reader     the reader to an underlying CSV source.
     * @param dialect    the dialect to use to parse the CSV.
     * @param fieldNames the list of field names.
     *
     * @.precond fieldNames must contains one name per column (not less).
     */
    public DictReader(java.io.Reader reader, Dialect dialect,
            String[] fieldNames) {
        final CSVParser parser = new CSVParserBuilder()
        		.withSeparator(dialect.delimiter)
        		.withEscapeChar(dialect.escape)
        		.withQuoteChar(dialect.quotechar)
        		.withIgnoreQuotations(true)
        		.build();
        this.reader = new CSVReaderBuilder(reader)
        		.withCSVParser(parser)
        		.build();
        this.fieldNames = fieldNames;
    }

    /**
     * Reads the entire file into a List where each row being a Map of
     * {@literal <field name, value>}.
     *
     * @return a List of Map<String, String>, where each field is indexed by
     * the field name.
     *
     * @throws IOException if something goes wrong during the read.
     * @throws CSVException if the number of field names is different of
     * number of columns.
     * @throws CsvException 
     */
    public List<Map<String, String>> readAll() throws IOException, CSVException, CsvException {
        List<String[]> rawData = this.reader.readAll();
        List<Map<String, String>> rows = null;

        if(rawData != null) {
            if (rawData.get(0).length != fieldNames.length)
            	throw new CSVException("Number of field names is different of number of columns");

            rows = new ArrayList<Map<String, String>>(rawData.size());
            for (int i = 0; i < rawData.size(); i++) {
                String[] fields = rawData.get(i);
                Map<String, String> row = new HashMap <String, String>();

                for (int j = 0; j < fields.length; j++) {
                    row.put(fieldNames[j], fields[j]);
                }
                rows.add(i, row);
            }
        }
        return rows;
    }

    /**
     * Reads the next line from the file.
     *
     * @param multiline sets if the quoted entries can span multiple lines.
     *
     * @return a map where fields are indexed by field name.
     *
     * @throws IOException if something goes wrong during the read.
     * @throws CSVException if the number of field names is different of
     * number of columns.
     * @throws CsvValidationException 
     */
    public Map<String, String> readRow() throws IOException, CSVException, CsvValidationException {
        String[] rawData = this.reader.readNext();
        Map<String, String> row = null;

        if(rawData != null) {
            // Check the precondition.
            if (rawData.length != fieldNames.length)
            	throw new CSVException("Number of field names is different of number of columns");

            row = new HashMap <String, String>();
            for (int i = 0; i < rawData.length; i++) {
                row.put(fieldNames[i], rawData[i]);
            }
        }
        return row;
    }

    /**
     * Returns the list of field names.
     * @return the list of field names.
     */
    public String[] headers() {
    	return fieldNames;
    }

    /**
     * Closes the underlying reader.
     *
     * @throws IOException if the close fails.
     */
    @Override
	public void close() throws IOException {
        reader.close();
    }

    /** The CSV reader. */
    private CSVReader reader;
    /** The header. */
    private String[] fieldNames;
}
