package fr.inrae.urep.format.csv;

import java.io.Closeable;
import java.io.IOException;
import java.util.List;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvException;
import com.opencsv.exceptions.CsvValidationException;

/**
 * This class implements a simple CSV reader that can be used to "slurp" the
 * whole file in one time or to use it in an Iterator style pattern.
 * @author INRA UREP
 */

/*
 * This class is a simple wrapper for the CSVReader of OpenCSV.
 * http://opencsv.sourceforge.net/
 */
public class Reader implements Closeable {
    /**
     * Constructs Reader that uses the default dialect.
     * 
     * @param reader the reader to an underlying CSV source.
     */
    public Reader(java.io.Reader reader) {
        this(reader, new Dialect());
    }

    /**
     * Constructs Reader with supplied dialect.
     * 
     * @param reader  the reader to an underlying CSV source.
     * @param dialect the dialect to use to parse the CSV.
     */
    public Reader(java.io.Reader reader, Dialect dialect) {
        final CSVParser parser = new CSVParserBuilder()
        		.withSeparator(dialect.delimiter)
        		.withEscapeChar(dialect.escape)
        		.withQuoteChar(dialect.quotechar)
        		.withIgnoreQuotations(true)
        		.build();
        this.reader = new CSVReaderBuilder(reader)
        		.withCSVParser(parser)
        		.build();
    }

    /**
     * Reads the entire file into a List where each row being a String[] of
     * fields.
     * 
     * @param multiline sets if the quoted entries can span multiple lines.
     *      
     * @return a List of String[], with each String[] representing a row.
     * 
     * @throws IOException if something goes wrong during the read.
     * @throws CsvException 
     */
    public List<String[]> readAll() throws IOException, CsvException {
        return reader.readAll();
    }

    /**
     * Reads the next line from the file.
     * 
     * @param multiline sets if the quoted entries can span multiple lines.
     * 
     * @return a string array with one column per cell.
     * 
     * @throws IOException if something goes wrong during the read.
     * @throws CsvValidationException 
     */
    public String[] readRow() throws IOException {
    	try{
    		return reader.readNext();
    	}catch (CsvValidationException e) {
			throw new IOException(e.getMessage());
		}
    }

    /**
     * Closes the underlying reader.
     * 
     * @throws IOException if the close fails.
     */
    public void close() throws IOException{
        reader.close();
    }

    /** The CSV reader. */
    private CSVReader reader;
}
