package fr.inrae.urep.format.csv;

import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.opencsv.CSVWriter;
import com.opencsv.CSVWriterBuilder;
import com.opencsv.ICSVWriter;

/**
 * This class implements a CSV writer that operates like the Writer class but
 * maps dictionaries onto output rows.
 * @author INRA UREP
 */
public class DictWriter implements Closeable {
    /**
     * Constructs DictWriter with supplied field names and default dialect.
     * 
     * @param writer     the writer to an underlying CSV source.
     * @param fieldNames the list of field names. Gives the order in which
     * values in the map are written.
     * 
     * @.precond fieldNames must contains one name per column (not less).
     */
    public DictWriter(java.io.Writer writer, String[] fieldNames) {
        this(writer, new Dialect(), fieldNames);
    }

    /**
     * Constructs DictWriter with supplied field names and default dialect.
     * 
     * @param writer     the writer to an underlying CSV source.
     * @param dialect    the dialect to use to write the CSV.
     * @param fieldNames the list of field names. Gives the order in which
     * values in the map are written.
     * 
     * @.precond fieldNames must contains one name per column (not less).
     */
    public DictWriter(java.io.Writer writer, Dialect dialect,
            String[] fieldNames) {
    	this.writer = (CSVWriter) new CSVWriterBuilder(writer)
    		    .withSeparator(dialect.delimiter)
    		    .withQuoteChar(dialect.quotechar)
    		    .withEscapeChar(dialect.escape)
    		    .withLineEnd(ICSVWriter.DEFAULT_LINE_END)
    		    .build(); // will produce a CSVWriter
        this.fieldNames = fieldNames;
    }

    /**
     * Writes the header (field names) into a file.
     */
    public void writeHeader() {
        writer.writeNext(fieldNames);
    }

    /**
     * Writes a List of rows into a file.
     * 
     * @param rows a List of Map<String, String>, where each field is indexed by
     * the field name.
     */
    public void writeAll(List<Map<String, String>> rows) {
        writeHeader();
        for (Map<String, String> row : rows) {
            writeRow(row);
        }
    }

    /**
     * Writes a row into a file.
     * 
     * @param row a map where fields are indexed by field name.
     */
    public void writeRow(Map<String, String> row) {
        String[] linearRow = new String[row.size()];
        for (int i = 0; i < row.size(); i++) {
            linearRow[i] = row.get(fieldNames[i]);
        }
        writer.writeNext(linearRow);
    }

    /**
     * Closes the underlying writer.
     * 
     * @throws IOException if the close fails.
     */
    public void close() throws IOException {
        writer.close();
    }

    /** The CSV writer. */
    private CSVWriter writer;
    /** The header. */
    private String[] fieldNames;
}
