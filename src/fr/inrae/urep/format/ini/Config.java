package fr.inrae.urep.format.ini;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class provides a parser, in standard Java, for the INI-like
 * configuration files. It provides the necessary methods to deal with the
 * configuration files. It allows read existing file as well creating new.
 * @author INRA UREP
 */
public class Config {
	/** Creates an empty configuration, using '=' as delimiter. */
    public Config() {
        conf = new LinkedHashMap<String, Map<String, String>>();
        this.delimiter = '=';
    }

	/**
	 * Creates an empty configuration using the specified delimliter.
	 * @param delimiter The field separator string. Items are separated from
	 * values by this character.
	 */
    public Config(char delimiter) {
        this();
        this.delimiter = delimiter;
    }

    /**
     * Creates a configuration object from an existing file, using the default
     * encoding.
     * @param path File path.
     * @param delimiter The field separator string. Options are separated from
     * values by this character.
     * @throws IOException If an I/O error occurs reading from the file or a
     * malformed or unmappable byte sequence is read.
     * @throws INIException If the INI-file is malformed.
     */
    public Config(String path, char delimiter)
    		throws IOException, INIException {
    	this(path, delimiter, Charset.defaultCharset().name());
    }

    /**
     * Creates a configuration object from an existing file, using '=' as
     * delimiter.
     * @param path File path.
     * @param encoding The encoding to be used on the file.
     * @throws IOException If an I/O error occurs reading from the file or a
     * malformed or unmappable byte sequence is read.
     * @throws INIException If the INI-file is malformed.
     */
    public Config(String path, String encoding)
    		throws IOException, INIException {
    	this(path, '=', encoding);
    }

    /**
     * Creates a configuration object from an existing file, using the default
     * encoding and '=' as delimiter.
     * @param path File path.
     * @throws IOException If an I/O error occurs reading from the file or a
     * malformed or unmappable byte sequence is read.
     * @throws INIException If the INI-file is malformed.
     */
    public Config(String path) throws IOException, INIException {
    	this(path, '=', Charset.defaultCharset().name());
    }

    /**
     * Creates a configuration object from an existing file.
     * @param path File path.
     * @param delimiter The field separator string. Options are separated from
     * values by this character.
     * @param encoding The encoding to be used on the file.
     * @throws IOException If an I/O error occurs reading from the file or a
     * malformed or unmappable byte sequence is read.
     * @throws INIException If the INI-file is malformed.
     */
    public Config(String path, char delimiter, String encoding)
    		throws IOException, INIException {
        this(delimiter);
        List<String> lines = Files.readAllLines(Paths.get(path),
                                                Charset.forName(encoding));
        String sectionName = null;
        int lineno = 0;
        try {
        	for (Iterator<String> it = lines.iterator(); it.hasNext();) {
        		String line = it.next();
        		lineno += 1;
        		if (!isComment(line)) {
        			if (isSection(line)) {
        				sectionName = line.split("\\[|\\]")[1];
        				addSection(sectionName);
        			}
        			else if (isOption(line, delimiter)) {
        				Pattern regexp = Pattern.compile("\\s*" + delimiter + "\\s*");
        				Matcher match = regexp.matcher(line);
        				match.find();
        				int start = match.start();
        				int end = match.end();
        				String name = line.substring(0, start);
        				// Remove leading spaces.
        				name = name.replaceFirst("^\\s+", "");
        				String value = line.substring(end, line.length());
        				if (value.startsWith("'") || value.startsWith("\"")) {
        					value = parseQuotedValue(value, it);
        				}
        				else {
        					value = parseUnquotedValue(value, it);
        				}
        				addOption(sectionName, name, value);
        			}
        		}
        	}
        }
        catch(INIException err) {
        	throw new INIException("Line " + lineno + ": " + err.getMessage());
        }
    }

    /**
     * Returns the list of sections.
     * @return the list of sections.
     */
    public List<String> sections() {
        return new ArrayList<String>(conf.keySet());
    }

    /**
     * Returns the list of options in the specified section.
     * @param section Name of the section inside which to search the options.
     * @return the list of options in the specified section.
     * @throws INIException
     */
    public List<String> options(String section) throws INIException {
    	if (!hasSection(section)) {
			throw new INIException("Section '" + section +
					               "' does not exist.");
		}
        return new ArrayList<String>(conf.get(section).keySet());
    }

    /**
     * Tests the existence of a section in the configuration.
     * @param section Section name.
     * @return true if the section exist, otherwise false.
     */
    public boolean hasSection(String section) {
        return conf.containsKey(section);
    }

    /**
     * Tests the existence of an option in a section of the configuration.
     * @param section Name of the section inside which to test the existence of
     * the option.
     * @param option Option name.
     * @return true if the option exist in the section, otherwise false.
     * @throws INIException If the section does not exist.
     */
    public boolean hasOption(String section, String option)
    		throws INIException {
    	if (!hasSection(section)) {
			throw new INIException("Section '" + section +
					               "' does not exist.");
		}
        return conf.get(section).containsKey(option);
    }

    /**
     * Returns the value, as string, associated to the option.
     * @param section Section name.
     * @param option Option name.
     * @return the value, as string, associated to the option.
     * @throws INIException If the section and/or the item does not exist.
     */
    public String get(String section, String option) throws INIException {
    	if (!hasOption(section, option)) {
			throw new INIException("Option '" + option + "' does not exist " +
					               "in section '" + section + "'.");
		}
        return conf.get(section).get(option);
    }

    /**
     * Adds a section to the configuration.
     * @param section Section name.
     * @throws INIException If the section already exist.
     */
    public void addSection(String section) throws INIException {
    	if (hasSection(section)) {
			throw new INIException("Section '" + section + "' already exist.");
		}
        conf.put(section, new LinkedHashMap<String, String>());
    }

    /**
     * Adds an option to the section of a configuration.
     * @param section Name of the section inside which to create the option.
     * @param option Option name.
     * @param value Option value.
     * @throws INIException If the section does not exist or if the section
     * already contains an option with the same name
     */
    public void addOption(String section, String option, Object value)
    		throws INIException {
    	if (hasOption(section, option)) {
			throw new INIException("Option '" + option + "' already exist " +
    	                           "in section '" + section + "'.");
		}
        conf.get(section).put(option, value.toString());
    }

    /**
     * Removes a section from the configuration.
     * @param section Name of the section to remove.
     * @throws INIException If the section does not exist.
     */
    public void removeSection(String section) throws INIException {
    	if (!hasSection(section)) {
			throw new INIException("Section '" + section +
					               "' does not exist.");
		}
    	conf.get(section).clear();
    	conf.remove(section);
    }

    /**
     * Removes an option from a section of a configuration.
     * @param section Name of the section inside which to delete the option.
     * @param option Name of the option to remove.
     * @throws INIException If the section does not exist and/or if the option
     * does not exist.
     */
    public void removeOption(String section, String option)
    		throws INIException {
    	if (!hasOption(section, option)) {
			throw new INIException("Option '" + option + "' does not exist " +
		                           "in section '" + section + "'.");
		}
    	conf.get(section).remove(option);
    }

    /**
     * Writes a configuration object into a file, using the default encoding.
     * @param path File path.
     * @throws IOException If an I/O error occurs writing to or creating the
     * file, or the text cannot be encoded using the specified charset.
     */
    public void write(String path) throws IOException {
    	write(path, Charset.defaultCharset().name());
    }

    /**
     * Writes a configuration object into a file.
     * @param path File path.
     * @param encoding The encoding to be used on the file.
     * @throws IOException If an I/O error occurs writing to or creating the
     * file, or the text cannot be encoded using the specified charset.
     */
    public void write(String path, String encoding) throws IOException {
        String[] lines = toString().split(System.lineSeparator());
        Files.write(Paths.get(path), Arrays.asList(lines),
        		    Charset.forName(encoding));
    }

    @Override
    public String toString() {
    	String eol = System.lineSeparator();
    	StringBuilder builder = new StringBuilder();
    	for (String section : sections()) {
    		builder.append('[' + section + ']' + eol);
    		try {
				for (String option : options(section)) {
			    	String value = get(section, option);

			    	// If the value is only space and/or tab character, quote it.
			    	// If the value starts like a comment, quote it.
			    	if (value.trim().isEmpty() || value.startsWith("#")
			    			                   || value.startsWith(";"))
			    		value = '"' + value + '"';

					builder.append(option + delimiter + value + eol);
				}
			} catch (INIException err) {
				// If it happens, that means something goes terribly wrong...
				err.printStackTrace();
			}
		}
    	return builder.toString();
    }

    /**
     * Tests if the line is a comment.
     * @param line Line to test.
     * @return true if the line is a comment, otherwise false.
     */
    private boolean isComment(String line) {
    	Pattern regexp = Pattern.compile("^\\s*(#|;).*$");
    	return regexp.matcher(line).matches();
    }

    /**
     * Tests if the line is a section declaration.
     * @param line Line to test.
     * @return true if the line is a section declaration, otherwise false.
     */
    private boolean isSection(String line) {
    	Pattern regexp = Pattern.compile("^\\s*\\[.+\\].*$");
    	return regexp.matcher(line).matches();
    }

    /**
     * Tests if the line is an option declaration.
     * @param line Line to test.
     * @param sep The field separator string. Items are separated from values
     * by this character.
     * @return true if the line is an option declaration, otherwise false.
     */
    private boolean isOption(String line, char sep) {
    	Pattern regexp = Pattern.compile("^.+\\s*" + sep + "\\s*.+$");
    	return regexp.matcher(line).matches();
    }

    /**
     * Parses the option value.
     * @param value Current value of the option.
     * @param it Iterator on the lines.
     * @return the option value.
     * @throws INIException If no matching quotation mark is found before the
     * end of the file.
     */
    private String parseQuotedValue(String value, Iterator<String> it)
    		throws INIException {
    	StringBuilder sb = new StringBuilder();
    	char quote = value.charAt(0); // Extract the quoting character.
    	value = value.substring(1);   // Remove the first quoting character.
		boolean endQuote = false;
		// We search a quoting character that is not preceded by a backslash
		// or a quoting character alone on a line.
    	Pattern regexp = Pattern.compile("([^\\\\]" + quote +
    			                         ")|(^" + quote + "$)");
    	try {
    		while (!endQuote) {
    			Matcher match = regexp.matcher(value);
    			if (match.find()) {
    				sb.append(value.substring(0, match.end() - 1));
    				endQuote = true;
    			}
    			else {
    				sb.append(value + System.lineSeparator());
        			value = it.next();
    			}
    		}
    	}
    	catch (NoSuchElementException err) { // We reach the end of the file.
    		throw new INIException("Un-terminated quoted field");
    	}

    	String quotedValue = sb.toString();
    	// Unescape the escaped quotes.
    	quotedValue = quotedValue.replaceAll("\\\\(" + quote + ")", "$1");
    	// Unescape the escaped escape character (\).
    	return quotedValue.replaceAll(Matcher.quoteReplacement("\\\\"),
    				                  Matcher.quoteReplacement("\\"));
    }

    /**
     * Parses the option value.
     * @param value Current value of the option.
     * @param it Iterator on the lines.
     * @return the option value.
     * @throws INIException If the multiline value does not end before the end
     * of the file.
     */
    private String parseUnquotedValue(String value, Iterator<String> it)
    		throws INIException {
    	StringBuilder sb = new StringBuilder();

		// Remove trailing spaces and inline comments.
		value = value.replaceFirst("\\s*((#|;).*)?$", "");
    	try {
    		while (value.endsWith("\\")) {
    			// Remove trailing \
    			value = value.trim().substring(0, value.length() - 1);
    			sb.append(value);
    			value = it.next();
    			// Remove trailing spaces and inline comments.
    			value = value.replaceFirst("\\s*((#|;).*)?$", "");
    		}
    	}
    	catch (NoSuchElementException err) { // We reach the end of the file.
    		throw new INIException("Non-ended multiline field");
    	}

    	return sb.append(value.trim()).toString();
    }

    /**
     * This is a two-level hash table.
     * The first hash table stores the sections, each section is a hash table
     * that stores.
     */
    private Map<String, Map<String, String>> conf;
    /**
     * The field separator string. Items are separated from values
     * by this character.
     */
    private char delimiter;
}
