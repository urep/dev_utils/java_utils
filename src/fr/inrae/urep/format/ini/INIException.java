package fr.inrae.urep.format.ini;

/**
 * This class signals that an INI-exception of some sort has occurred.
 * @author INRA UREP
 */
public class INIException extends Exception {
	/** Constructs a new exception with null as its detail message. */
	public INIException() {
		super();
	}

	/**
	 * Constructs a new exception with the specified detail message. The cause
	 * is not initialized, and may subsequently be initialized by a call to
	 * Throwable.initCause(java.lang.Throwable).
	 * @param message The detail message. The detail message is saved for later
	 * retrieval by the Throwable.getMessage() method.
	 */
	public INIException(String message) {
		super(message);
	}

	/**
	 * Constructs a new exception with the specified detail message and cause.
	 *
	 * Note that the detail message associated with cause is not automatically
	 * incorporated in this exception's detail message.
	 * @param message The detail message. The detail message is saved for later
	 * retrieval by the Throwable.getMessage() method.
	 * @param cause The cause (which is saved for later retrieval by the
	 * Throwable.getCause() method). (A null value is permitted, and indicates
	 * that the cause is nonexistent or unknown.)
	 */
	public INIException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Constructs a new exception with the specified cause and a detail message
	 * of (cause==null ? null : cause.toString()) (which typically contains the
	 * class and detail message of cause). This constructor is useful for
	 * exceptions that are little more than wrappers for other throwables (for
	 * example, PrivilegedActionException).
	 * @param cause The cause (which is saved for later retrieval by the
	 * Throwable.getCause() method). (A null value is permitted, and indicates
	 * that the cause is nonexistent or unknown.)
	 */
	public INIException(Throwable cause) {
		super(cause);
	}

	/** Serial version unique identifier. */
	private static final long serialVersionUID = 3048409832018792498L;
}
