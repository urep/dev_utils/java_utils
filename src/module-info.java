module java_utils {
    exports fr.inrae.urep.widget;
    exports fr.inrae.urep.meteo;
    exports fr.inrae.urep.format.netcdf;
    exports fr.inrae.urep.math;
    exports fr.inrae.urep.format.ini;
    exports fr.inrae.urep.format.csv;
    exports fr.inrae.urep;
    exports fr.inrae.urep.math.rand;

    requires com.opencsv;
    requires java.desktop;
    requires java.sql;
}